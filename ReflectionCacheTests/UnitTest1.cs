using NUnit.Framework;
using ReflectionCache;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CachedReflectionInfoTest()
        {
            var reflectionTypeWrapper = new CachedReflectionInfoReflectionTypeWrapper();
            var castMethod = reflectionTypeWrapper.GetMethod("All_TSource_2");
            Assert.NotNull(castMethod);
        }
    }
}