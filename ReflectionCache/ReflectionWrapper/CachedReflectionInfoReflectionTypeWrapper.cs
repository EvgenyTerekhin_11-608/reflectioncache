namespace ReflectionCache
{
    public class CachedReflectionInfoReflectionTypeWrapper : ReflectionTypeWrapper
    {
        protected override string TypeAndAssemblyName =>
            "System.Linq.CachedReflectionInfo,System.Linq.Queryable, Version=4.0.3.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";
    }
}