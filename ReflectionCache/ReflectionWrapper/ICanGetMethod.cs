﻿using System.Reflection;

namespace ReflectionCache
{
    public interface ICanGetMethod
    {
        MethodInfo GetMethod(string methodName);
    }
}