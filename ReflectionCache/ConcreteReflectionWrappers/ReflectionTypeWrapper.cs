using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ReflectionCache
{
    public abstract class ReflectionTypeWrapper : ICanGetMethod
    {
        protected abstract string TypeAndAssemblyName { get; }
        private Type Type { get; set; }

        public ReflectionTypeWrapper()
        {
            Type = Type.GetType(TypeAndAssemblyName);
        }

        public MethodInfo GetMethod(string methodName)
        {
            var method = Type.GetMethod(methodName);
            if (method == null) throw new Exception("Такого метода не существует");
            return method;
        }
    }
}